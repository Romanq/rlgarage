﻿using Garage.Models;
using Garage.Models.Garage_parser;
using Newtonsoft.Json;
using RL_Garage.Garage_parser;
using RL_Garage.Insider_parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RLGarage.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public JsonResult Index()
        {
            PageParser pp = new PageParser();
            List<string> Trades = pp.Parse();
            var result = new { result = Trades };

            string json = JsonConvert.SerializeObject(result);

            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = json
            };
        }

        public ActionResult Database()
        {
            List<string> container = new Insider().Update();

            var result = new { result = container };

            string json = JsonConvert.SerializeObject(result);

            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = json
            };

        }

        public ActionResult Saved(int min, int max)
        {
            /*ViewBag.min = min;
            ViewBag.max = max;

            using (SaveTrades context = new SaveTrades())
            {
                var trades = context.SavedTrades.Where(q => (q.HavePrice - q.WantPrice) > min && (q.HavePrice - q.WantPrice) < max).ToList();

                trades.Reverse(); //Descending

                if (trades != null)
                    return View(trades);
                else return View();
            }*/

            return View();
        }

        [HttpPost]
        public JsonResult Blacklist(string Steamid)
        {
            /*ViewBag.Steamid = Steamid;

            using (Blacklist context = new Blacklist())
            {
                var entity = context.Bots.Where(q => q != null && q.Blacklink == Steamid).FirstOrDefault();

                if (entity != null)
                {
                    //Already exist
                    return new JsonResult() { Data = "Bot already exist" };
                }
                else
                {
                    context.Bots.Add(new Bot { Blacklink = Steamid });
                    context.SaveChanges();
                    return new JsonResult() { Data = "Successful updated" };
                }
            }*/

            return new JsonResult() { Data = "Temporary off" };
        }
    }
}