﻿using Garage.Models.Insider_parser;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Garage.Models
{
    public class EFDatabase : DbContext
    {
        public EFDatabase() : base("DbConnection")
        {

        }

        public DbSet<Bot> Bots { get; set; }

        public DbSet<DatabaseItem> DatabaseItems { get; set; }

    }
}