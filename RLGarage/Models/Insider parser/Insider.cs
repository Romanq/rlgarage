﻿using System.Net;
using HtmlAgilityPack;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data.Entity;
using Garage.Models.Insider_parser;
using System.Linq;
using System.Globalization;
using Garage.Models;
using System.Threading.Tasks;

namespace RL_Garage.Insider_parser
{
    public class Insider
    {
        #region Members

        private string Priceurl = "https://rl.insider.gg/pc?mobile";

        public List<string> Base = new List<string>();

        enum Colors
        {
            [StringValue("Default")]
            Default = 0,
            [StringValue("Black")]
            Black = 1,
            [StringValue("Titanium White")]
            Titanium_White = 2,
            [StringValue("Grey")]
            Grey = 3,
            [StringValue("Crimson")]
            Crimson = 4,
            [StringValue("Pink")]
            Pink = 5,
            [StringValue("Cobalt")]
            Cobalt = 6,
            [StringValue("Sky Blue")]
            Sky_Blue = 7,
            [StringValue("Burnt Sienna")]
            Burnt_Sienna = 8,
            [StringValue("Saffron")]
            Saffron = 9,
            [StringValue("Line")]
            Lime = 10,
            [StringValue("Forest Green")]
            Forest_Green = 11,
            [StringValue("Orange")]
            Orange = 12,
            [StringValue("Purple")]
            Purple = 13
        }

        #endregion
        
        public List<string> Update()
        {
            Base.Clear(); //List for database

            using (WebClient client = new WebClient())
            {
                string page = client.DownloadString(Priceurl);
                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(page);

                var Nodes = document.DocumentNode.SelectNodes("//div[starts-with(@id,'item')]");
                Nodes.RemoveAt(0);
                Nodes.RemoveAt(0);


                foreach (HtmlNode Node in Nodes)
                {

                    Item InsiderItem = new Item();
                    InsiderItem.Itemid = Convert.ToInt32(Regex.Match(Node.Id, @"\d+").Value);
                    InsiderItem.Itemname = Node.GetAttributeValue("data-itemfullname", "None");
                    string Prices = Node.GetAttributeValue("data-prices", "0").Replace("[", "").Replace("]", "").Replace("\"", "");
                    var ColorPrices = Prices.Split(',');

                    for (int i = 0; i < ColorPrices.Length; i++)
                    {
                        Colors c = (Colors)i;
                        InsiderItem.Prices.Add(new Tuple<string, string>(StringEnum.GetStringValue(c), ReturnPrice(ColorPrices[i])));
                    }

                    Base.AddRange(ReturnItemString(InsiderItem));

                }

            }
            return Base;
        }

        public static List<string> ReturnItemString(Item item)
        {
            List<string> result = new List<string>();

            for (int i = 0; i < item.Prices.Count; i++)
            {
                var TempPrice = item.Prices[i];
                List<string> MinMaxPrice = new List<string>();
                MinMaxPrice.AddRange(TempPrice.Item2.Split('-'));

                if (MinMaxPrice.Count < 2)
                {
                    if (MinMaxPrice[0] == "Null")
                        MinMaxPrice[0] = "0";
                    MinMaxPrice.Add("0");
                }

                int min = Convert.ToInt32(MinMaxPrice[0].Trim());
                int max = Convert.ToInt32(MinMaxPrice[1].Trim());
                
                
                DatabaseItem Dbitem = new DatabaseItem { Itemname = item.Itemname, Color = TempPrice.Item1, MinPrice = min, MaxPrice = max };

                using (EFDatabase context = new EFDatabase())
                {

                    var entity = context.DatabaseItems.Where(q => q.Itemname != null && q.Itemname == Dbitem.Itemname && q.Color == TempPrice.Item1).FirstOrDefault();

                    if (entity != null)
                    {
                        entity.MinPrice = Dbitem.MinPrice;
                        entity.MaxPrice = Dbitem.MaxPrice;
                        context.Entry(entity).State = EntityState.Modified;
                    }
                    else
                    {
                        context.DatabaseItems.Add(Dbitem);
                    }

                    context.SaveChanges();
                }

                result.Add($"{item.Itemname}: {TempPrice.Item1} Price {min}-{max}");
            }

            return result;
        }

        private string ReturnPrice(string expression)
        {
            expression = expression.Replace("&hairsp;", "");
            var elem = expression.Split('-');

            if (elem.Length < 2)
                return "Null";

            if (elem[1].Contains("k"))
            {
                var resultLeft = Regex.Match(elem[0], @"-?\d+(?:\.\d+)?").Value;
                var resultRight = Regex.Match(elem[1], @"-?\d+(?:\.\d+)?").Value;
                int left = (int)((double.Parse(resultLeft, CultureInfo.InvariantCulture)) * 1000);
                int right = (int)((double.Parse(resultRight, CultureInfo.InvariantCulture)) * 1000);
                return $"{left}-{right}";
            }

            return $"{elem[0]}-{elem[1]}";
        }

    }
}
