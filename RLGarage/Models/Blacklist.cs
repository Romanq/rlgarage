﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Garage.Models
{
    public class Blacklist : DbContext
    {
        public Blacklist() : base("DbConnection")
        {

        }

        public DbSet<Bot> Bots { get; set; }

    }

    public class Bot
    {
        public int Id { get; set; }
        public string Blacklink { get; set; }
    }
}