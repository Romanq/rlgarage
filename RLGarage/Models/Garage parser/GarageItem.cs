﻿namespace RL_Garage.Garage_parser
{
    public class GarageItem
    {
        public string Itemname { get; set; }
        public string Color { get; set; }
        public int CreditValue { get; set; }
        public int Count { get; set; }
    }
}
