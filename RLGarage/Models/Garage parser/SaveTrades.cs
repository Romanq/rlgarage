﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Garage.Models.Garage_parser
{
    public class SaveTrades : DbContext
    {
        public SaveTrades() : base("DbConnection")
        {
        }

        public DbSet<Trade> SavedTrades { get; set; }
    }

    public class Trade
    {
        public int Id { get; set; }
        public string Have { get; set; }

        public string Want { get; set; }

        public int HavePrice { get; set; }

        public int WantPrice { get; set; }

        public string TradeLink { get; set; }

        public string Platform { get; set; }
    }
}