﻿namespace RL_Garage.Garage_parser
{
    public class Links
    {
        public string InventoryLink { get; set; }
        public string Steam { get; set; }
        public string TradeLink { get; set; }
    }
}
